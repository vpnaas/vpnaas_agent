FROM python:3.10.7-alpine3.16

WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . .

EXPOSE 8000
ENTRYPOINT [ "gunicorn", "--threads=4", "--bind=0.0.0.0", "app:app" ]