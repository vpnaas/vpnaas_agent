from abc import ABC
import os
import docker


class VpnAgent(ABC):
    def create_client(self, name: str):
        raise NotImplementedError

    def get_client(self, name: str):
        raise NotImplementedError

    def get_status(self):
        raise NotImplementedError


class DockerVpnAgent(VpnAgent):
    def __init__(self) -> None:
        super().__init__()
        self._docker = None
        self.__connect_client()
        self._client_path = os.getenv('CLIENT_PATH', '/tmp/clients')

        self.__create_client_directory()

    def __connect_client(self):
        try:
            self._docker = docker.DockerClient(version='auto')
        except docker.errors.DockerException:
            pass

    def __create_client_directory(self):
        try:
            os.mkdir(self._client_path)
        except FileExistsError:
            pass

    def create_client(self, name: str):
        execute = self._docker.containers.get('openvpn').exec_run(
            f'easyrsa build-client-full {name} nopass')

        if execute.exit_code != 0:
            return False

        self.__client_save(name)
        return True

    def __client_save(self, name: str):
        execute = self._docker.containers.get(
            'openvpn').exec_run(f'ovpn_getclient {name}')
        with open(f'{self._client_path}/{name}.ovpn', 'w+') as f:
            f.write(execute.output.decode('utf-8'))

    def get_client(self, name: str):
        with open(f'{self._client_path}/{name}.ovpn', 'r') as f:
            return f.read()

    def get_client_list(self):
        clients = []
        for f in os.listdir(self._client_path):
            if f.endswith('.ovpn'):
                clients.append(f.removesuffix('.ovpn'))
        return clients

    def get_status(self):
        if self._docker is None:
            return 'docker not running'
        try:
            container = self._docker.containers.get('openvpn')
        except docker.errors.NotFound:
            return 'container not exists'
        return container.status
