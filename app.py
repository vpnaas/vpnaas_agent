from flask import Flask, jsonify
from flask_caching import Cache
from agent import DockerVpnAgent, VpnAgent

config = {
    "DEBUG": True,
    "CACHE_TYPE": "SimpleCache",
    "CACHE_DEFAULT_TIMEOUT": 300
}

app = Flask(__name__)
app.config.from_mapping(config)
cache = Cache(app)

agent: VpnAgent = DockerVpnAgent()


@app.route("/")
def status():
    return jsonify({
        'status': agent.get_status(),
    })


@app.route("/vpn", methods=['get'])
def list_vpn_client():
    vpn = cache.get('vpn')
    if vpn is None:
        vpn = agent.get_client_list()
        cache.set('vpn', vpn)

    return jsonify(vpn)


@app.route("/vpn/<client_name>", methods=['get'])
def get_vpn_client(client_name):
    f = agent.get_client(client_name)
    return jsonify({'file': f})


@app.route("/vpn/<client_name>", methods=['post'])
def create_vpn_client(client_name):
    created = agent.create_client(client_name)
    if not created:
        return jsonify({'status': 'nok'})
    cache.delete('vpn')
    return jsonify({'status': 'ok'})
